FROM alpine

RUN apk upgrade --update \
    && apk add --no-cache --update cntlm \
    && rm -rf /var/cache/apk/*

ENTRYPOINT ["cntlm"]
CMD ["-f"]
