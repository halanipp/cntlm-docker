# cntlm-docker

Docker application for running [`cntlm`](http://cntlm.sourceforge.net/).

This allows you to seamlessly authenticate with HL's proxy _without_ regularly handling your password in plaintext.

## Install

1. Run `./install.sh`.
2. Enter your normal HL password when prompted.
3. Done.

Whenever you change your HL password, repeat the procedure above.

## Usage

Run `./run.sh`.

This will start `cntlm` running in the background at `http://localhost:3128`. You can then connect your applications to this URL, e.g. curl, npm.

## Examples

```
# cURL
$ echo "--proxy http://localhost:3128" >> ~/.curlrc
$ curl https://www.google.com

# NPM
$ npm config set proxy=http://localhost:3128
$ npx cowsay moo
```
