#/bin/sh

# Grab user inputs
user=`whoami`
read -sp "[cntlm-docker] Please enter password for $user: " pw
echo -e "\n"

# Install cntlm
docker load --input dist/cet_cntlm.tar.gz

# Generate NTLM hash (not encrypted)
hash=`docker run --rm cet/cntlm -d HLDS -u $user -p $pw -H | grep "PassNTLMv2" | cut -b 17-48`

# Configure cntlm.conf file
cat cntlm.conf.template \
    | sed "s/<hl-username-goes-here>/$user/" \
    | sed "s/<hashed-hl-password-goes-here>/$hash   # Updated $(date)/" \
    > cntlm.conf
echo "Successfully generated file: cntlm.conf."
