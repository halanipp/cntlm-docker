#!/bin/sh

dir=`dirname $(realpath "$0")`

MSYS_NO_PATHCONV=1 docker run \
    --detach \
    --name cntlm \
    --publish 3128:3128 \
    --rm \
    --volume "$dir/cntlm.conf:/etc/cntlm.conf" \
    cet/cntlm > /dev/null

# Error handling
if [ $? != 0 ]; then
    exit 1
fi

# Success message
echo "Started cntlm in the background. 👏"
echo ""
echo "You can now use http://localhost:3128 as your proxy URL."
echo "To stop cntlm, run \`docker kill cntlm\`."
